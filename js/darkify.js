// This script gets run everytime a GitLab page gets loaded (any page under the domain *.gitlab.com)
// receive commands here to change colors and such

load("bg-color", result => {
    document.body.style.backgroundColor = result["bg-color"]
})

browser.storage.onChanged.addListener(
    changes => {
        for (let change in changes) {
            switch (change) {
                case "bg-color":
                    console.log(change)
                    document.body.style.backgroundColor = changes[change].newValue
                    break;
            }
        }
    }
)