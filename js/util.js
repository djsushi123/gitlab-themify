function load(key, onLoad) {
    browser.storage.local.get(key).then(onLoad)
}

function save(key, value) {
    let obj = {}
    obj[key] = value
    browser.storage.local.set(obj).then()
}