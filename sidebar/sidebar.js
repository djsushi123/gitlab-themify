function onChangeSetting(id, callback) {
    document.getElementById(id).addEventListener("change", callback)
}

// on sidebar open, we want to load all the values from DB
load("bg-color", result => {
    document.getElementById("input-bg-color").value = result["bg-color"]
})

onChangeSetting("input-bg-color", event => {
    save("bg-color", event.currentTarget.value)
    console.log(`bg-color changed to ${event.currentTarget.value}`)
})